<?php

include_once 'Animal.php';

/**
 * Class Maitre
 */
class Maitre
{
    /**
     * @var Animal
     */
    private $animal;

    /**
     * @return mixed
     */
    public function getAnimal()
    {
        return $this->animal;
    }

    /**
     * Le paramètre $animal doit être un objet de la classe Animal
     *
     * @param Animal $animal
     */
    public function setAnimal(Animal $animal)
    {
       $this->animal = $animal;
    }

    /**
     *
     */
    public function caresserAnimal()
    {
        if (!empty($this->animal)) {
            $this->animal->crier();
        }
    }
}