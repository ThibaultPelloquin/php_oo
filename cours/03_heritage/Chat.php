<?php
include_once 'Animal.php';

/**
 * Class Chat
 */
class Chat extends Animal
{
    /**
     * Surcharge de l'attribut espèce dans Chat
     *
     * @var string
     */
    protected $espece = 'chat';

    /**
     * Surcharge de la méthode identifier()
     * de la classe Animal
     *
     * @return string
     */
    public function identifier()
    {
        // parent fait référence à la classe mère
        return parent::identifier() . " mais je suis aussi un chat !";
    }


    /**
     * Chat doit implémenter cette méthode ou être déclaré abstrait
     * car la méthode crier est déclarée abstraite dans Animal
     */
    public function crier()
    {
        echo "Miaou !";
    }

    /**
     * Méthode finale
     * Ne peut pas être définie dans une classe fille
     */
    final public function ronronner()
    {
        echo 'Ronron';
    }


}
