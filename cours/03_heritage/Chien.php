<?php
include_once 'Animal.php';

/**
 * Class Chien
 */
class Chien extends Animal
{
    /**
     * Surcharge de l'attribut espèce dans Chat
     *
     * @var string
     */
    protected $espece = 'chien';

    public function crier()
    {
        echo "Ouaf !";
    }
}
