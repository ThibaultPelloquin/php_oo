<?php

/**
 * Class Animal
 *
 * Classe déclarée abstraite : elle n'est pas faite pour être instanciée seule
 */
abstract class Animal
{
    // public    : tout le monde
    // private   : personne à part la classe elle-même
    // protected : depuis la classe elle-même et les classes qui en héritent (via extends)
    protected $espece = 'indéfinie';

    public function getEspece()
    {
        return $this->espece;
    }

    public function identifier()
    {
        return "Je suis un animal";
    }

    /**
     * Méthode abstraite
     *
     * Cette méthode ne peut être déclarée abstraite que parce que sa classe est abstraite
     *
     * Toutes les classes qui héritent d'Animal doivent implémenter cette méthode
     *
     * @return string
     */
    abstract public function crier();
}
