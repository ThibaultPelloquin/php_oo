<?php

include_once 'Chat.php';
include_once 'Chien.php';
include_once 'Siamois.php';
include_once 'SiamoisAngora.php';
//include_once 'SiamoisAngora.php';

$chat  = new Chat();
$chien = new Chien();

// echo $chat->espece;

echo $chat->getEspece();

echo '<hr>';

echo $chien->getEspece();

// Fatal Error : Animal est une classe abstraite
// $animal = new Animal();

echo '<hr>';


echo $chat->identifier();
echo '<br>';
echo $chien->identifier();

echo '<hr>';

$chat->crier();
echo '<br>';
$chien->crier();

echo '<hr>';

// Retourne le nom de la classe de l'objet
echo get_class($chat);

var_dump($chat instanceof Chat);

// true car Chat hérite de Animal
var_dump($chat instanceof Animal);

$siamois = new Siamois();

echo 'get_class';
var_dump(get_class($siamois)); // Siamois
echo 'get_parent_class';
var_dump(get_parent_class($siamois)); // Chat
echo 'is_a';
var_dump(is_a($siamois, 'Chat')); // true
var_dump(is_a($siamois, 'Siamois')); // true
// instanceof permet de savoir si un objet est d'une classe donnée
echo 'instanceof';
var_dump($siamois instanceof Siamois); // true
var_dump($siamois instanceof Chat);    // true
var_dump($siamois instanceof Animal);  // true

//$siamois2 = new SiamoisAngora();

$siamois->ronronner();
