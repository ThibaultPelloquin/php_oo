<?php
include_once 'Chat.php';

/**
 * Class Siamois
 *
 * Final signifique qu'on ne peut pas la surcharger
 */
final class Siamois extends Chat
{
    /**
     *  FATAL ERROR : la méthode ronronner() est déclarée finale dans Chat
     */
    /*public function ronronner()
    {
        echo 'Ronron ronron';
    }*/

}
