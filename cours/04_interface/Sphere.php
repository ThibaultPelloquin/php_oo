<?php
include_once 'Volume.php';

/**
 * Class Sphere
 */
class Sphere implements Volume
{

    public function getForme()
    {
        return 'sphère';
    }

}