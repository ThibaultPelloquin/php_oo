<?php
require_once 'Objet.php';

/**
 * Class Objet
 *
 * On peut implémenter plusieurs interfaces en les séparant par des virgules
 */
class Objet implements Texture, Volume
{
    /**
     * @var
     */
    protected $couleur;

    /**
     * @var
     */
    protected $matiere;

    /**
     * @var
     */
    protected $forme;

    // La classe doit obligatoirement contenir les méthodes getCouleur() et getMatiere()
    // car elle implémente l'interface Texture

    /**
     * @return mixed
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * @param $couleur
     * @return $this
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatiere()
    {
        return $this->matiere;
    }

    /**
     * @param $matiere
     * @return $this
     */
    public function setMatiere($matiere)
    {
        $this->matiere = $matiere;

        return $this;
    }

    // La classe doit obligatoirement contenir la méthode getForme car elle implémentaire l'interface volume

    /**
     * @return mixed
     */
    public function getForme()
    {
        return $this->forme;
    }

    /**
     * @param $forme
     * @return $this
     */
    public function setForme($forme)
    {
        $this->forme = $forme;

        return $this;
    }




}
