<?php

/**
 * Interface Texture
 */
interface Texture
{
    public function getMatiere();

    public function getCouleur();
}