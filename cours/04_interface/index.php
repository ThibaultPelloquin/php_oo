<?php

include_once 'Jouer.php';
include_once 'Texture.php';
include_once 'Cube.php';
include_once 'Sphere.php';
include_once 'De.php';

function getFormeVolume(Volume $volume)
{
    return $volume->getForme();
}

$cube   = new Cube();
$sphere = new Sphere();

var_dump($cube instanceof Volume);

echo 'Forme du cube : ' . getFormeVolume($cube);

echo '<br>';

echo 'Forme de la sphère : ' . getFormeVolume($sphere);

echo '<hr>';

$de = new De('bois', 'blanc');

echo 'Matière du dé : ' . $de->getMatiere();

echo '<br>';

echo 'Couleur du dé : ' . $de->getCouleur();

echo '<br>';

echo 'Lançons 6 fois le dé : ' . '<br>';
$score = 0;
for ($i = 1; $i <= 6; $i++) {
    $resultat = $de->lancer();
    $score += $resultat;
    echo "Jet n° $i : " . $resultat;
    echo '<br>';
}
echo 'Score total : ' . $score;
