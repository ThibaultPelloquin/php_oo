<?php

/**
 * Class De
 *
 * On peut implémenter autant d'interfaces que souhaité
 */
class De extends Cube implements Texture, Jouer
{
    private $couleur;
    private $matiere;

    public function __construct($matiere, $couleur)
    {
        $this->matiere = $matiere;
        $this->couleur = $couleur;
    }

    public function getCouleur()
    {
        return $this->couleur;
    }

    public function getMatiere()
    {
        return $this->matiere;
    }

    public function lancer()
    {
        return rand(1,6);
    }

}
