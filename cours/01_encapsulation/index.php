<?php

setlocale(LC_ALL, 'fr_FR');

include_once './Person.php';

$thibault = new Person(new DateTime('1985-06-19'));

// FATAL ERROR. Tentative d'accès à une propriété privée
// $thibault->lastname = 'Thibault';
$thibault->setFirstname('Pelloquin');
$thibault->setLastname('Thibault');

/*try {
    $thibault->setLastname(array('Trololol'));
} catch (Exception $e) {
    echo $e->getMessage();
}*/

// Les parenthèses permettent d'appliquer une méthode à la voler
$mrTrololo = (new Person('1934-09-04'))
    ->setFirstname('Édouard')
    // chainage direct grâce au return $this de setFirstname
    ->setLastname('Khil')
;

?>
<ul>
    <li>Nom : <?= $thibault->getFirstname(); ?></li>
    <li>Prénom : <?= $thibault->getLastname(); ?></li>
    <li>Date de naissance : <?= $thibault->getBirthday(); ?> (<?= $thibault->getAge(); ?> ans)</li>
</ul>

<?php

var_dump($mrTrololo);

// FATAL ERROR. Tentative d'accès à une propriété privée.
var_dump($thibault->birthday->format('Y-m-d'));
