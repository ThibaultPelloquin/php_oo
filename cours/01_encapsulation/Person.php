<?php

/**
 * Class Person
 *
 * Le principe de l'encapsulation est de définir l'accessibilité des propriétés/méthodes de la classe :
 *  - public : accessible depuis l'extérieur
 *  - private : accessible uniquement depuis la classe
 *  - protected : accessible uniquement depuis la classe et ses enfants (voir partie Héritage)
 *
 * Problème posé par l'encapsulation en PHP. L'accessibilité est obligatoirement bilattérale :
 *  - si l'attribut est public, il est accessible en lecture et en écriture.
 *  - si l'attribut est privé, il est inaccessible en lecture et en écriture.
 *
 * Avantages et inconvénients :
 *  - En accessibilité publique :
 *     - les propriétés sont facilement accessibles et modifiables...
 *     - et sont donc facilement alterables...
 *  - En accessibilité privée :
 *     - les propriétés ne peuvent être définies et atteintes que via des méthodes intermédiaires
 *       appelées les getter/setter
 *
 * Les getter et setter sont conventionnels.
 *  - setPropertyName()
 *  - getPropertyName()
 *
 * Sous PhpStorm, une fois les attributs renseignés, on peut les générer automatiquement en lançant
 * [Alt] + [Ins] à l'intérieur de la classe (de même pour le constructeur)
 */
class Person
{
    // Les attributs, ici tous privés

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var \DateTime
     */
    private $birthday;

    // Le constructeur

    /**
     * Person constructor.
     *
     * Le constructeur est une méthode magique qui simplifie l'instanciation de la classe :
     *  - en permettant d'y placer directement des propriétés en paramètres
     *  - en y appliquant des actions
     *
     * @param int|string|\DateTime $birthday timestamp, date au format "Y-m-d" ou \DateTime
     * @param string $firstname
     * @param string $lastname
     */
    public function __construct($birthday, $lastname = 'X', $firstname = 'Bidule')
    {
        // On pourrait les définir directement
        // $this->birthday = $birthday;
        // $this->lastname = $lastname;
        // $this->firstname = $firstname;

        // Utilisation des setter
        $this->setBirthday($birthday);
        $this->setFirstname($firstname);
        $this->setLastname($lastname);
    }

    // Les Setters

    /**
     * Par définition un setter est public
     *
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname)
    {
        // On peut en profiter pour placer une validation qui empêche de mettre n'importe quoi dans la propriété
        if (is_string($firstname) && strlen($firstname) > 0) {
            $this->firstname = $firstname;
        }

        // Il est de bon ton, à la fin des setter, de retourner $this
        // Cela permet le chaînage des définitions de propriétés
        return $this;
    }

    /**
     * @param string $lastname
     * @return $this
     * @throws Exception
     */
    public function setLastname($lastname)
    {
        if (is_string($lastname) && strlen($lastname) > 0) {
            $this->lastname = $lastname;
        }

        return $this;
    }

    /**
     * @param $birthday
     * @return $this
     */
    public function setBirthday($birthday)
    {
        // On peut aussi se servir d'un setter pour laisser plus de liberté au développeur
        if (is_a($birthday, 'DateTime')) { // DateTime
            $this->birthday = $birthday;
        } elseif ( // Date au format 'Y-m-d'
            is_string($birthday)
            && \DateTime::createFromFormat("Y-m-d", $birthday) !== false
        ) {
            $this->birthday = new \DateTime($birthday);
        }

        return $this;
    }

    // Les Getters

    /**
     * Tout comme le setter, par définition le getter est public
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * On peut se servir d'un getter pour renvoyer une propriété dans un format directement human friendly
     *
     * @return string
     */
    public function getBirthday()
    {
        return strftime('%A %e %B %Y', $this->birthday->getTimestamp());
    }

    // Les méthodes publiques de la classe

    /**
     * Méthode de classe
     *
     * Une méthode est une fonction interne à la classe
     *
     * @return string
     */
    public function getFullName()
    {
        return "$this->firstname $this->lastname";
    }

    /**
     *
     * @function getAge
     * @return int Âge en année
     */
    public function getAge()
    {
        return $this->birthday->diff(new DateTime('today'))->y;
    }
}
