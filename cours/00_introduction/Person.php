<?php

/**
 * Class Person
 *
 * Par convention :
 *   - le nom de la classe commence par une majuscule
 *   - le nom du fichier est identique au nom de la classe, en en respectant la casse
 *   - le fichier de la classe ne contient que la classe (une seule classe par fichier, rien à l'extérieur)
 */
class Person
{
    // Les attributs ou propriétés de classe
    // Par convention, tout attribut utilisable doit être déclaré en tête de classe

    /**
     * Attribut avec une valeur par défaut
     *
     * @var string
     */
    public $firstname = 'Machin';

    /**
     * Attribut sans valeur par défaut
     *
     * @var string
     */
    public $lastname;

    // Les méthodes de la classe suivent les attributs

    /**
     * Méthode de classe
     *
     * Une méthode est une fonction interne à la classe
     *
     * @return string
     */
    public function getFullName()
    {
        // La pseudo-variable $this représente la classe en tant qu'instance
        // L'opérateur -> appelé opérateur objet sert à accéder à une propriété ou une méthode de l'objet
        return "$this->firstname $this->lastname";
    }
}
