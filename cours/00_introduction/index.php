<?php

setlocale(LC_ALL, 'fr_FR');

include_once './Person.php';

// Instanciation
$jean = new Person();

// Définition d'une propriété publique
$jean->firstname = 'Jean';
$jean->lastname = 'd\'Ormesson';

?>
<ul>
    <li>Nom : <?= $jean->firstname; // Accès direct à une propriété public ?></li>
    <li>Prénom : <?= $jean->lastname; ?></li>
</ul>

<p>Nom complet&nbsp;: <?= $jean->getFullName(); // Appel d'une méthode publique ?></p>

<?php

var_dump($jean);
