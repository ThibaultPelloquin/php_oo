<?php

/**
 * Class Triangle
 */
class Triangle
{
    /**
     * Définition d'une constante de classe
     *
     * Non modifiable, d'aucune manière.
     * Non encapsulable (toujours accessible en lecture)
     *
     * Par convention, en majuscules
     */
    const NB_ANGLES = 3;

    /**
     * Autre constante
     */
    const NB_COTES = 3;

    /**
     * Autre constante
     */
    const TOTAL_ANGLES = 180;

    /**
     * Nombre de triangles instanciés
     *
     * Attribut statique avec une valeur par défaut
     * Cet attribut est le même partout
     * Il est possible d’accéder à ces propriétés et méthodes sans avoir à créer d’instance de classe
     *
     * @var int
     */
    public static $nbTriangles = 0;

    /**
     * La même qu'au dessus mais privée, donc sécurisable
     *
     * @var int
     */
    private static $nbTrianglesSecured = 0;

    /**
     * Triangle constructor.
     *
     * La méthode de constructeur n'est lancée qu'à l'instanciation new Class();
     * Elle n'est donc pas lancé lorsqu'on appelle la classe seule Class::...
     */
    public function __construct()
    {
        self::$nbTriangles += 1;
        self::$nbTrianglesSecured += 1;
    }

    /**
     * Triangle destructor.
     *
     * Méthode lancée lorsqu'une instance est détruite
     */
    public function __destruct()
    {
        self::$nbTriangles -= 1;
        self::$nbTrianglesSecured -= 1;

        echo 'Il reste ' . self::$nbTrianglesSecured . ' triangles.' . "\n";
    }

    /**
     * Méthode statique de type Getter mais pour les attributs statiques
     *
     * @return int
     */
    public static function getNbTriangles()
    {
        // self fait référence à la classe elle-même
        // alors que $this fait référence à l'objet instancié de la classe !
        return self::$nbTrianglesSecured;
    }

    /**
     * ERROR
     * $this fait référence à un objet instancié : ça n'a donc aucun sens
     *
     * @return $this
     */
    public static function dummy()
    {
        return $this;
    }
}
