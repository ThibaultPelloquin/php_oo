<?php

include_once 'Triangle.php';

// appel d'une constante de classe via l'opérateur de résolution de portée '::'
// le '::' est aussi appelé "Paamayim Nekudotayim" (hébreux)

echo '<pre>';

// Affichage d'une constante de classe
echo 'Triangle::NB_ANGLES : ' . Triangle::NB_ANGLES . "\n";

// Affichage d'une propriété publique statique sans instanciation
echo 'Triangle::$nbTriangles : ' . Triangle::$nbTriangles . " (avant instanciations)\n"; // 0

// Instanciation de cinq triangles
$triangleA = new Triangle();
$triangleB = new Triangle();
$triangleC = new Triangle();
$triangleD = new Triangle();
$triangleE = new Triangle();

// Idem plus haut
echo 'Triangle::$nbTriangles : ' . Triangle::$nbTriangles . " (après instanciation)\n"; // 3
// La propriété statique publique est aussi accessible depuis les instanciations
echo '$triangleB::$nbTriangles : ' . $triangleB::$nbTriangles . "\n"; // 3

// DANGER si l'attribut statique est publique, tout comme les attributs classiques, il est modifiable directement
Triangle::$nbTriangles = 'Nawak';

echo 'Triangle::$nbTriangles : ' . Triangle::$nbTriangles . " (après modification directe)\n"; // 3

echo 'Triangle::$getNbTriangles : ' . Triangle::getNbTriangles() . "\n";

// Destruction d'un des triangles (histoire de voir l'application de __destruct)
unset($triangleC);

// FATAL ERROR : privé et sécurisé !
// echo Triangle::$nbTrianglesSecured;
// NOTICE : undefined variable "$this"
var_dump($triangleA->dummy());

echo 'On est arrivé en fin de script' . "\n";

// Les triangles restant sont donc automatiquement détruits.
