<?php

/**
 * Trait Dumper
 */
trait Dumper
{
    protected $useLessProperty = 'DummyDumper';

    /**
     * @param mixed $var
     */
    public function dump($var)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }

    public function autoDump()
    {
        echo '<pre>';
        var_dump($this);
        echo '</pre>';
    }
}
