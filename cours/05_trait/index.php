<?php

setlocale(LC_ALL, 'fr_FR');

include_once './Dater.php';
include_once './Dumper.php';
include_once './Person.php';

$didier = (new Person(new DateTime('1973-02-27')))
    ->setFirstname('Didier')
    ->setLastname('Super')
;

$didier->autoDump();
