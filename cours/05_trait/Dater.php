<?php

/**
 * Trait Dater
 */
trait Dater
{
    /**
     * @param DateTime|string $date
     * @return bool|DateTime
     */
    public function getValidDateTime($date)
    {
        if (is_a($date, 'DateTime')) { // DateTime
            $dateTime = $date;
        } elseif ( // Date au format 'Y-m-d'
            is_string($date)
            && \DateTime::createFromFormat("Y-m-d", $date) !== false
        ) {
            $dateTime = new \DateTime($date);
        } else {
            $dateTime = false;
        }

        return $dateTime;
    }
}
