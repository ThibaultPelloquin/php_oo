<?php

/**
 * Class Person
 */
class Person
{
    // On appelle le ou les traits en haut de la classe
    use Dater;
    use Dumper;

    // Les attributs

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var \DateTime
     */
    private $birthday;

    // Le constructeur

    /**
     * Person constructor.
     *
     * @param int|string|\DateTime $birthday timestamp, date au format "Y-m-d" ou \DateTime
     * @param string $firstname
     * @param string $lastname
     */
    public function __construct($birthday, $lastname = 'X', $firstname = 'Bidule')
    {
        $this->setBirthday($birthday);
        $this->setFirstname($firstname);
        $this->setLastname($lastname);
    }

    // Les Setters

    /**
     * Par définition un setter est public
     *
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname)
    {
        // On peut en profiter pour placer une validation qui empêche de mettre n'importe quoi dans la propriété
        if (is_string($firstname) && strlen($firstname) > 0) {
            $this->firstname = $firstname;
        }

        return $this;
    }

    /**
     * @param string $lastname
     * @return $this
     * @throws Exception
     */
    public function setLastname($lastname)
    {
        if (is_string($lastname) && strlen($lastname) > 0) {
            $this->lastname = $lastname;
        }

        return $this;
    }

    /**
     * @param string|DateTime $birthday
     * @return $this
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $this->getValidDateTime($birthday);

        return $this;
    }

    // Les Getters

    /**
     * Tout comme le setter, par définition le getter est public
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * On peut se servir d'un getter pour renvoyer une propriété dans un format directement human friendly
     *
     * @return string
     */
    public function getBirthday()
    {
        return strftime('%A %e %B %Y', $this->birthday->getTimestamp());
    }

    // Les méthodes publiques de la classe

    /**
     * Méthode de classe
     *
     * Une méthode est une fonction interne à la classe
     *
     * @return string
     */
    public function getFullName()
    {
        return "$this->firstname $this->lastname";
    }

    /**
     *
     * @function getAge
     * @return int Âge en année
     */
    public function getAge()
    {
        return $this->birthday->diff(new DateTime('today'))->y;
    }
}
