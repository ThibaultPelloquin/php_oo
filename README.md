# PHP OO #

Petite introduction rapide au PHP Orienté Objet

## Y'a quoi dedans !? ##

* Bah j'viens d'le dire, hé banane ! Une petite introduction rapide au PHP Orienté Objet.
* Version 0.1.1
* [Dépôt sur Bitbucket.org](https://bitbucket.org/ThibaultPelloquin/php_oo/)

## Ouais mais plus précisément ? ##

### Une partie cours ###

* 00_introduction
  * Notion de classe et d'objet
  * Notion de propriété = attribut
  * Notion de méthode
  * Notion d'instance
* 01_encapsulation
  * Notion d'encapsulation
  * Notions d'attributs/propriétés et méthodes privées/publiques
* 02_static
  * Constante de classe
  * Propriété et méthode statique
  * Accès à une propriété ou à une méthode statique
  * Différence classe et objet
* 03_heritage
  * Notion d'héritage : extends, parent
  * Notion de classes et méthodes abstraites
  * Notion de classes et méthodes finales
  * Tester qu'un objet est une instance d'une classe, etc.
* 04_interface
  * Interface
* 05_trait
  * Trait
